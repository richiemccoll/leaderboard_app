PlayersList = new Mongo.Collection('players');

if (Meteor.isClient){
    
    Meteor.subscribe('thePlayers');
  
    Template.addPlayerForm.events({
      'submit form': function(event){
      event.preventDefault();
      var playerNameVar = event.target.playerName.value;
      Meteor.call('insertPlayerData', playerNameVar);
      }
    });  
  
    Template.leaderboard.helpers({
            'player': function(){
              var currentUserId = Meteor.userId();
              return PlayersList.find({}, {sort: {score: -1, name: 1}})
            },
            'selectedClass': function(){
              var playerID = this._id;
              var selectedPlayer = Session.get('selectedPlayer');
              if (playerID == selectedPlayer){
                  return 'selected'
              }},
             'showSelectedPlayer': function(){
               var selectedPlayer = Session.get('selectedPlayer');
               return PlayersList.findOne(selectedPlayer)
             }
            
});
    Template.leaderboard.events({
      'click .player': function(){
        var playerId = this._id;
        Session.set('selectedPlayer', playerId);
      },
      'click .increment': function(){
        var selectedPlayer = Session.get('selectedPlayer');
        Meteor.call('modifyPlayerScore', selectedPlayer, 5);
      },
      'click .decrement': function(){
        var selectedPlayer = Session.get('selectedPlayer');
        Meteor.call('modifyPlayerScore', selectedPlayer, -5);
      },
      'click .remove': function(){
        if(confirm("Are you sure?")) {
        var selectedPlayer = Session.get('selectedPlayer');
        Meteor.call('removePlayerData', selectedPlayer);
        }
    else
    {
        console.log('Did not delete user')
    }    
      }
    });
}

if (Meteor.isServer){
  
    Meteor.methods({
    'insertPlayerData': function(playerNameVar){
      var currentUserId = Meteor.userId();
      PlayersList.insert({
          name: playerNameVar,
          score: 0,
          createdBy: currentUserId
      });
     },
     'removePlayerData': function(selectedPlayer){
       var currentUserId = Meteor.userId(); 
       PlayersList.remove({_id: selectedPlayer, createdBy: currentUserId});
     },
      'modifyPlayerScore': function(selectedPlayer, scoreValue){
        PlayersList.update(selectedPlayer, {$inc: {score: scoreValue} });
      }
  });
  
  Meteor.publish('thePlayers', function(){
    var currentUserId = this.userId;      
    return PlayersList.find({createdBy: currentUserId })
          });
}

